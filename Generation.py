import hashlib 

class Generation:

    def __init__(self, login, md5Pass):
        self.log = login
        self.hashedPass = md5Pass

    # Mot correspondant à un mot de la liste (variantes maj/min autorisées)
    def wordFromList(self, wordList):
        idx = 0

        for word in wordList:
            nbLetters = len(word)
            charList = list(word)

            # On cherche maintenant les variantes maj/min
            for i in range(0,nbLetters):
                charList[i] = charList[i].upper()
                
                for letter in charList:
                    charList[idx] = letter.lower()
                    if(hashlib.md5("".join(charList).encode()).hexdigest().upper() == self.hashedPass.upper()):
                        return "".join(charList)
                            
                    charList[idx] = letter.upper()
                    if(hashlib.md5("".join(charList).encode()).hexdigest().upper() == self.hashedPass.upper()):
                        return "".join(charList)
                    idx+=1
                idx = 0
                
                charList[i] = charList[i].lower()

                for letter in charList:
                    charList[idx] = letter.upper()
                    if(hashlib.md5("".join(charList).encode()).hexdigest().upper() == self.hashedPass.upper()):
                        return "".join(charList)
                                
                    charList[idx] = letter.lower()
                    if(hashlib.md5("".join(charList).encode()).hexdigest().upper() == self.hashedPass.upper()):
                        return "".join(charList)
                    idx+=1
                idx = 0
        return False

    # loginNumber || numberLogin
    def loginNumber(self):
        
        # Utilisé pour mettre la première lettre du login en maj
        charList = list(self.log)
        upperLogin = charList
        upperLogin[0] = charList[0].upper()

        for i in range(0,10000):
            # Assemble possibility login+i
            assembledPass = str(self.log) + str(i)            
            encodedTry = hashlib.md5(assembledPass.encode()) 

            if(encodedTry.hexdigest().upper() == self.hashedPass.upper()):
                return assembledPass
            
            # Assemble possibility Login+i
            assembledPass = "".join(upperLogin) + str(i)            
            encodedTry = hashlib.md5(assembledPass.encode()) 

            if(encodedTry.hexdigest().upper() == self.hashedPass.upper()):
                return assembledPass
            
            # Assemble 2nd possibility i+login
            assembledPass = str(i) + str(self.log)            
            encodedTry = hashlib.md5(assembledPass.encode()) 

            if(encodedTry.hexdigest().upper() == self.hashedPass.upper()):
                return assembledPass

            # Assemble possibility i+Login
            assembledPass = str(i) + "".join(upperLogin)       
            encodedTry = hashlib.md5(assembledPass.encode()) 

            if(encodedTry.hexdigest().upper() == self.hashedPass.upper()):
                return assembledPass

            # Assemble 3rd possibility login+0[0..3]+i
            if(i<10):
                assembledPass = str(self.log) + "000" + str(i)
            elif(i<100):
                assembledPass = str(self.log) + "00" + str(i)
            elif(i<1000):
                assembledPass = str(self.log) + "0" + str(i)
            
            encodedTry = hashlib.md5(assembledPass.encode()) 
            
            if(encodedTry.hexdigest().upper() == self.hashedPass.upper()):
                return assembledPass

            # Assemble possibility Login+0[0..3]+i
            if(i<10):
                assembledPass = "".join(upperLogin)+ "000" + str(i)
            elif(i<100):
                assembledPass = "".join(upperLogin) + "00" + str(i)
            elif(i<1000):
                assembledPass = "".join(upperLogin) + "0" + str(i)           
            encodedTry = hashlib.md5(assembledPass.encode()) 

            if(encodedTry.hexdigest().upper() == self.hashedPass.upper()):
                return assembledPass
                
        return ""
    
    # (Voyelle = Nombre) .upper
    def vowelsAreNumbers(self, wordList):
        for word in wordList:
            charList = list(word)
            upperWord = ""

            # On met le tout en majuscule
            idxChar = 0
            for i in charList:
                upperWord+=charList[idxChar].upper()
                idxChar+=1
                
            for j in range(0,10):                
                assembledPass = ""
                for letter in upperWord:
                    if(letter == 'A' or letter == 'E' or letter == 'I' or letter == 'O' or letter == 'U' or letter == 'Y'):
                        assembledPass+=str(j)
                    else:
                        assembledPass+=str(letter)
                    
                if(hashlib.md5("".join(assembledPass).encode()).hexdigest().upper() == self.hashedPass.upper()):
                    return "".join(assembledPass)

        return False

    # Mot à l'envers
    def reversedWord(self, wordList):
        reversedList = list(wordList)
        idx = 0 

        # Reverse les mots
        for word in reversedList:
            reversedList[idx] = reversedList[idx][::-1]
            idx+=1

        for word in reversedList:
            # Assemble possibility i
            assembledPass = word
            encodedTry = hashlib.md5(assembledPass.encode()) 

            if(encodedTry.hexdigest().upper() == self.hashedPass.upper()):
                return assembledPass
        
        return False 
    
    # Mot dédoublé
    def doubledLetters(self, wordList):
        for word in wordList:
            idx = 1
            charList = list(word)
            for letter in word:
                charList.insert(idx,letter)
                idx += 2

            # Assemble possibility i
            assembledPass = "".join(charList)
            encodedTry = hashlib.md5(assembledPass.encode()) 

            if(encodedTry.hexdigest().upper() == self.hashedPass.upper()):
                return assembledPass

        return False

    # Mot correspondant à la concatenation de 2 chaînes
    def concatenateWords(self, wordList):
        for word in wordList:
            for secWord in wordList:
                # Assemble possibility i
                assembledPass = word + secWord
                encodedTry = hashlib.md5(assembledPass.encode()) 

                if(encodedTry.hexdigest().upper() == self.hashedPass.upper()):
                    return assembledPass
        return False
    
    # Mot liste + Numbers
    def wordNumber(self, wordList):
        for word in wordList:            
            # Utilisé pour mettre la première lettre du login en maj
            charList = list(word)
            upperWord = charList
            upperWord[0] = charList[0].upper()

            for i in range(0,10000):
                # Assemble possibility word+i
                assembledPass = str(word) + str(i)
                encodedTry = hashlib.md5(assembledPass.encode()) 

                if(encodedTry.hexdigest().upper() == self.hashedPass.upper()):
                    return assembledPass

                # Assemble possibility Word+i
                assembledPass = "".join(upperWord) + str(i)            
                encodedTry = hashlib.md5(assembledPass.encode()) 
                
                if(encodedTry.hexdigest().upper() == self.hashedPass.upper()):
                    return assembledPass
                
                # Assemble 2nd possibility i+word
                assembledPass =  str(i) + str(word)          
                encodedTry = hashlib.md5(assembledPass.encode()) 

                if(encodedTry.hexdigest().upper() == self.hashedPass.upper()):
                    return assembledPass

                # Assemble possibility i+Word
                assembledPass = str(i) + "".join(upperWord)       
                encodedTry = hashlib.md5(assembledPass.encode()) 

                if(encodedTry.hexdigest().upper() == self.hashedPass.upper()):
                    return assembledPass
            
        return False