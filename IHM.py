from tkinter import *
from Event import *

class IHM(Frame):

    def __init__(self, fenetre):

        self.event = Event()
        Frame.__init__(self,fenetre)
        self.pack(fill=BOTH)

        self.messageLog=Label(self, text="Login : ")
        self.messageLog.grid(row=1, column=1)

        self.loginEntry = Entry(self, width=20)
        self.loginEntry.grid(row=1, column=2, padx=10, pady=10)
        
        self.messageMdp=Label(self, text="Mdp crypté : ")
        self.messageMdp.grid(row=1, column=3)

        self.mdpEntry = Entry(self, width=20)
        self.mdpEntry.grid(row=1, column=4, padx=10, pady=10)

        var = IntVar()
        var.set(1)
        Radiobutton(self, text = "2 mots de la liste concaténés", variable = var, value = 1).grid(row=2, column=2, columnspan=2)  
        Radiobutton(self, text = "Login + 1 à 4 chiffres ou inversement", variable = var, value = 2).grid(row=3, column=2, columnspan=2)
        Radiobutton(self, text = "Mot de la liste", variable = var, value = 3).grid(row=4, column=2, columnspan=2)
        Radiobutton(self, text = "Mot de la liste à l'envers", variable = var, value = 4).grid(row=5, column=2, columnspan=2)
        Radiobutton(self, text = "Mot de la liste dédoublé", variable = var, value = 5).grid(row=6, column=2, columnspan=2)
        Radiobutton(self, text = "Mot de la liste + 1 à 4 chiffres ou inversement", variable = var, value = 6).grid(row=7, column=2, columnspan=2)
        Radiobutton(self, text = "Mot de la liste où voyelles = chiffre", variable = var, value = 7).grid(row=8, column=2, columnspan=2)

        Button(self, text='Ouvrir un fichier', command=self.event.uploadAction).grid(row=9, column=2, columnspan=2, pady=10)
        Button(self, text='Trouver le mot de passe', command= lambda: self.event.generate(self.loginEntry.get(), self.mdpEntry.get(), var.get(), self.decryptedPass)).grid(row=10, column=2, columnspan=2, pady=10)

        Label(self, text="Mdp déchiffré : ").grid(row=11, column=2)
        self.decryptedPass = Entry(self, width=20)
        self.decryptedPass.grid(row=11, column=3)

        Button(self, text="Quitter", command=self.quit).grid(row=12, sticky=W, pady=10, padx=10)
    
