from tkinter import filedialog, END
from Generation import *

class Event:    
    
    def __init__(self):
        self.words = list()

    # Ouvre et stocke la liste de mots
    def uploadAction(self, event=None):
        filename = filedialog.askopenfilename()
        with open(filename, 'r') as fh:
            for line in fh:
                line = line.rstrip("\n")
                self.words.append(line.strip())

    # Récupère la liste de mots
    def getWordList(self):
        return self.words

    # Génère le mot de passe
    def generate(self, login, mdp, rbuttonVal, entry):
        if(login != "" and mdp != ""):
            generation = Generation(login, mdp)
            if(rbuttonVal == 2):
                entry.delete(0,END)
                entry.insert(0,generation.loginNumber())
            
            if(len(self.words) != 0 ):
                if(rbuttonVal == 1):
                    entry.delete(0,END)
                    entry.insert(0,generation.concatenateWords(self.words))
                
                if(rbuttonVal == 3):
                    entry.delete(0,END)
                    entry.insert(0,generation.wordFromList(self.words))
                
                if(rbuttonVal == 4):
                    entry.delete(0,END)
                    entry.insert(0,generation.reversedWord(self.words))
                
                if(rbuttonVal == 5):
                    entry.delete(0,END)
                    entry.insert(0,generation.doubledLetters(self.words))
                
                if(rbuttonVal == 6):
                    entry.delete(0,END)
                    entry.insert(0,generation.wordNumber(self.words))
                
                if(rbuttonVal == 7):
                    entry.delete(0,END)
                    entry.insert(0,generation.vowelsAreNumbers(self.words))

            elif(rbuttonVal!=2): 
                print("PAS DE FICHIER OUVERT !")     

        else:
            print("PAS DE MDP OU DE LOGIN RENSEIGNE !")     

        return 0